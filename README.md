# As a developer from Brazil, I want to go to...

## Australia

- [Google](https://careers.google.com/jobs/results/?company=Google&company=Google%20Fiber&company=YouTube&employment_type=INTERN&hl=en_US&jlo=en_US&q=&sort_by=relevance): roles for interns, new graduates and experienced professionals in Sydney. Levels: Bachelors, Masters and PhDs;

## Canada

- [AlayaCare](https://www.alayacare.com/careers): roles for experienced professionals in Vancouver. Levels: Bachelors and Masters;
- [Google](https://careers.google.com/jobs/results/?company=Google&company=Google%20Fiber&company=YouTube&employment_type=INTERN&hl=en_US&jlo=en_US&q=&sort_by=relevance): roles for interns, new graduates and experienced professionals in Kitchener, Montreal and Toronto. Levels: Bachelors, Masters and PhDs;
- [Microsoft](https://careers.microsoft.com/students/us/en/search-results): roles for interns, new graduates and experienced professionals in Vancouver. Levels: Bachelors, Masters and PhDs;
- [Shopify](https://www.shopify.com/careers/search?keywords=&sort=specialty_asc): roles for interns, new graduates and experienced professionals in multiple locations. Levels: Bachelors and Masters;

## Netherlands

- [Google](https://careers.google.com/jobs/results/?company=Google&company=Google%20Fiber&company=YouTube&employment_type=INTERN&hl=en_US&jlo=en_US&q=&sort_by=relevance): roles for interns, new graduates and experienced professionals in Amsterdam. Levels: Bachelors, Masters and PhDs;

## United Kingdom

- [Facebook](https://www.facebook.com/careers/jobs): roles for interns, new graduates and experienced professionals in London. Levels: Bachelors, Masters and PhDs;
- [Palantir](https://www.palantir.com/careers/): roles for interns, new graduates and experienced professionals in London. Levels: Bachelors and Masters;
- [Secret Escapes](https://careers.secretescapes.com/): roles for new graduates and experienced professionals in London. Levels: Bachelors and Masters;

## United States

- [Amazon](https://www.amazon.jobs/en-gb/business_categories/student-programs): roles for interns, new graduates and experienced professionals in Seattle, New York City and others. Levels: Masters and PhDs;
- [Cadence](https://cadence.wd1.myworkdayjobs.com/External_Careers/0/refreshFacet/318c8bb6f553100021d223d9780d30be): roles for new graduates and experienced professionals in multiple locations. Levels: Bachelors and Masters;
- [Google](https://careers.google.com/jobs/results/?company=Google&company=Google%20Fiber&company=YouTube&employment_type=INTERN&hl=en_US&jlo=en_US&q=&sort_by=relevance): roles for interns, new graduates and experienced professionals in multiple locations such as California, Illinois, Colorado and others. Levels: Bachelors, Masters and PhDs;
- [Microsoft](https://careers.microsoft.com/students/us/en/search-results): roles for interns, new graduates and experienced professionals in Washington. Levels: Bachelors, Masters and PhDs;
- [Palantir](https://www.palantir.com/careers/): roles for interns, new graduates and experienced professionals in Washington, New York City and others. Levels: Bachelors and Masters;



